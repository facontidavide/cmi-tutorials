/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <math.h>
#include "OS/PeriodicTask.h"
#include "cmi/CMI.h"
#include "tutorials.h"
#include "cmi/MAL_CANOpen402.h"

using namespace CanMoveIt;

class MyTask: public PeriodicTask
{
public:
    void startHook();
    void updateHook();
    void stopHook()
    {
        printf("end\n");
    }
};

MotorControllerPtr motor;


int cycle = 0;  // cycle counter

int main()
{
    Milliseconds PERIOD(18);

    try{
        CreateObjectDictionary(OBJ_DICTIONARY_FILE, NAME_DICTIONARY);

        // the xml file contain all the setup we need
        cmi_init(CAN_DRIVER, MAL_CONFIG_XML);

        // get the pointer of the motor with index 0.
        // Instance of motors where created reading VenusTest.xml
        motor = cmi_getMotorPtr(0);

        MyTask task;
        task.start_execution(PERIOD);
        task.wait_completion();
    }
    catch( Exception& exc)
    {
        std::cout<< exc.displayText() << std::endl;
        std::cout<< "\n\nPress Enter to continue" <<std::endl;
        getchar();
        return 1;
    }

    cmi_close();

    return 0;

}// end of main

#define PI 3.14159265359

void MyTask::startHook()
{
    // must be called once at the beginning
    cmi_configureMotor(motor);

    // set the mode of operation. See CO402_def.h for more options.
    cmi_setModeOperation(motor, INTERPOLATED_POSITION_MODE);

    motor->setCurrentLimit(1000);
    motor->setInterpolatedPositionPeriod( Milliseconds(20) ); // convert to milliseconds

    printf("--------------------------------------\n");
}


void  MyTask::updateHook()
{
    double actual_pos;

    // you need this Sync message to get the update of data mapped to PDOs.
    // This includes Actual Position;
    cmi_sendSync();

    sleep_for( Milliseconds(5));

    // get the last value of Actual position.
    // Note: it is updated everytime that a Sync is send (see end of this function)
    motor->getActualPosition( &actual_pos );

    // generate a sinusoidal position
    double AMPLITUDE = 25; // these are radians!!
    double period = duration_cast<Seconds>(getPeriod()).count();
    double t = period * cycle;
    double reference_pos =   AMPLITUDE*( 1.0-cos( t ) ) / 2.0;

    // send the command
    motor->pushInterpolatedPositionTarget( reference_pos );

    // dispaly the actual position 10 times per second
    if(cycle%10 == 0 )
    {
        printf("pos ACT %.2f  REF %.2f\n", actual_pos, reference_pos);
    }

    // kill the program after 2 full motion cycles, i.e. 12.56 seconds
    if( t >= PI*2)
    {
        this->stop_execution();
    }

    cycle++;
}
// end of loopFunc






