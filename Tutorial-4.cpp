/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "OS/PeriodicTask.h"
#include "cmi/CO301_interface.h"
#include "cmi/MAL_CANOpen402.h"
#include "cmi/CMI.h"
#include "tutorials.h"

using namespace CanMoveIt;
MotorControllerPtr _motor[2];

unsigned PHASE = 0; //PHASE counter

class MyTask: public PeriodicTask
{
public:
    FILE *fout;

    void startHook();
    void updateHook();
    void stopHook()  { fclose(fout); }
private:

};

//typedef boost::function< void(EventData const&)> EventCallback;

void OnTargetReached(EventData const& event)
{
    PHASE++;
    double actual_pos;
    _motor[event.device_id ]->getActualPosition(  &actual_pos );

    // id is the ID of the motor that you can use to get the motor pointer with  cmi_getMotorPtr(id)
    printf("-----Motor[%d]: target reached at pos %.3f: new PHASE %d-----\n", event.device_id, actual_pos , PHASE);

}



int main()
{
    try{
        CreateObjectDictionary(OBJ_DICTIONARY_FILE, NAME_DICTIONARY);

        // the xml file contain all the setup we need
        cmi_init(CAN_DRIVER, MAL_CONFIG_XML);

        MyTask task;
        task.start_execution( Milliseconds(100) );

        task.wait_completion();
    }
    catch( Exception& exc)
    {
        std::cout<< exc.displayText() << std::endl;
        std::cout<< "\n\nPress Enter to continue" <<std::endl;
        getchar();
        return 1;
    }
    return 0;
}

void MyTask::startHook()
{
    for(int i=0; i<2; i++)
    {
        _motor[i] = cmi_getMotorPtr(i);

        cmi_configureMotor(_motor[i]);

        _motor[i]->setModeOperation( PROFILED_POSITION_MODE);

        _motor[i]->setProfileParameter( PROFILE_ACCELERATION, 100);
        _motor[i]->setProfileParameter( PROFILE_DECELERATION, 100);
        _motor[i]->setProfileParameter( PROFILE_VELOCITY,50);

        _motor[i]->setProfiledPositionTarget( 0 );
    }
    sleep_for( Seconds(5));

    for(int i=0; i<2; i++)
    {
        _motor[i]->events()->add_subscription(EVENT_PROFILED_POSITION_REACHED,  CALLBACK_SYNCH, OnTargetReached );
    }
    PHASE = 0;
    fout = fopen("tutorial4-output.txt","w");
}


void MyTask::updateHook()
{
    cmi_sendSync();

    if (PHASE == 0) // first PHASE
    {
        printf("\n------------------------------------\n\n");
        _motor[0]->setProfiledPositionTarget( 50 );
        _motor[1]->setProfiledPositionTarget( 100 );
        PHASE ++;
    }


    if (PHASE == 3) // second PHASE
    {
        _motor[0]->setProfiledPositionTarget( 150 ,  PROFILE_RELATIVE_POS);
        _motor[1]->setProfiledPositionTarget( 100 ,  PROFILE_RELATIVE_POS);
        PHASE++;
    }


    if (PHASE  == 6)
    {
        _motor[0]->setProfiledPositionTarget(  -120, PROFILE_BUFFERED_POINT | PROFILE_RELATIVE_POS);
        _motor[0]->setProfiledPositionTarget(  -80,  PROFILE_BUFFERED_POINT | PROFILE_RELATIVE_POS);

        _motor[1]->setProfiledPositionTarget(  -120, PROFILE_BUFFERED_POINT | PROFILE_RELATIVE_POS);
        _motor[1]->setProfiledPositionTarget(  -80,  PROFILE_BUFFERED_POINT | PROFILE_RELATIVE_POS);
        PHASE++;
    }

    if (PHASE >= 11)
    {
        printf("complete\n");
        this->stop_execution();
    }

    double actual_pos[2];
    static int count=0;
    _motor[0]->getActualPosition(  &actual_pos[0] );
    _motor[1]->getActualPosition(  &actual_pos[1] );
    printf(".. %d .. PHASE %d  pos %.2f       %.2f\n",count++,PHASE, actual_pos[0], actual_pos[1]);

    // fprintf(fout, "%lld %f\n", time_since_epoch<Milliseconds>(), actual_pos);

}





