# this one is important
SET(CMAKE_SYSTEM_NAME Linux)

SET(ARMSTONE true)

# IMPORTANT: please change the following path
SET( COMPILER_PATH   absolute_path_of_your_toolchain)
SET( TOOLCHAIN_PATH  absolute_path_of_your_toolchain)

# specify the cross compiler
SET(CMAKE_C_COMPILER    ${COMPILER_PATH}/bin/arm-fs-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER  ${COMPILER_PATH}/bin/arm-fs-linux-gnueabi-g++)



# where is the target environment
SET(CMAKE_FIND_ROOT_PATH ${COMPILER_PATH}/ )

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
