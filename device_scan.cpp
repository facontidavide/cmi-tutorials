/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "cmi/CAN.h"
#include "cmi/CAN_Interface.h"
#include "cmi/CAN_driver.h"
#include "cmi/CMI.h"
#include "cmi/CO301_interface.h"
#include "cmi/CO402_def.h"

using namespace CanMoveIt;


int main(int argc, char** argv)
{
    std::string dictionary_file("../etc/ingenia_venus.eds");


    if (argc==2)
        dictionary_file = std::string(argv[1]);

    try{
#ifdef WIN32
        std::string can_driver("../lib/driver_peak_win32.dll");
        char busname[] = "PCAN_PCIBUS1";
#else
        std::string can_driver("../lib/libdriver_socket.so");
        char busname[] = "can0";
#endif
        // load the driver stored in a dynamic library (included with CAN/MoveIt).
        // The second argument is the name of a config file.
        // NULL means that no configuration files is needed for this project.
        LoadCanDriver( can_driver );
        CreateObjectDictionary( dictionary_file, "my_dictionary");

        printf("reading done\n");
        // open the CAN port on you master with the correct name and baudrate
        CANPort *can_port = canOpen(busname,"1M");

        printf("opening done\n");

        for (int i=1; i<=127;i++)
        {
            usleep(10*1000);
            // create a device with ID = i (second argument)
            try{
                CO301_InterfacePtr device = create_CO301_Interface( can_port, i,  "my_dictionary");
                printf("---------------------------\n" );
                printf("node %d: answer received. \n",i );
                printf("---------------------------\n" );
            }
            catch(SystemException)
            {
                printf("Node %d is NOT connected\n", i);
            }
            catch( Exception &e)
            {
                printf("something is wrong: %s\n", e.what());
            }
        }
        canClose(can_port);
    }
    catch( Exception& exc)
    {
        std::cout<< exc.displayText() << std::endl;
        return 1;
    }

    return 0;
}
