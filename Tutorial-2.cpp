/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "cmi/CO301_interface.h"
#include "cmi/CMI.h"
#include "OS/PeriodicTask.h"
#include "tutorials.h"

using namespace CanMoveIt;


class MyTask: public PeriodicTask
{
public:
    CO301_InterfacePtr motor;
    CANPort* can_port;
    EventPtr ev1, ev2;

    void startHook();
    void updateHook();
    void stopHook();
};

int main(int argc, char **argv)
{
    MyTask task;

    // this is the node that identify the device (slave)
    int DEVICE_NODE_IT = DEFAULT_NODE_ID;

    if (argc == 2)  DEVICE_NODE_IT = atoi(argv[1]);
    if( DEVICE_NODE_IT <1 || DEVICE_NODE_IT>128)
    {
        printf("wrong NODE_ID number\n");
        return 1;
    }

    std::cout << "!!!Hello CAN/MoveIt!!!" << std::endl;

    try{
        // load the driver. 30 is the thread priority (needed only on real-time systems)
        LoadCanDriver( CAN_DRIVER );

        // open the CAN port with the correct name and baudrate
        task.can_port = canOpen( BUSNAME ,"1M");

        // we start a motor controller using:
        // 1) the CAN port we opened,
        // 2) node id 3
        // 3) the  dictionary
        CreateObjectDictionary( OBJ_DICTIONARY_FILE,
                                NAME_DICTIONARY);

        task.motor = create_CO301_Interface( task.can_port,
                                             DEVICE_NODE_IT ,
                                             NAME_DICTIONARY);

        Milliseconds PERIOD(100);
        task.start_execution(PERIOD);

        task.wait_completion();
    }
    catch( Exception& exc)
    {
        std::cout<< exc.displayText() << std::endl;
        std::cout<< "\n\nPress Enter to continue" <<std::endl;
        getchar();
        return 1;
    }
    std::cout<< "\n\nPress Enter to continue" <<std::endl;
    getchar();
    return 0;
} // end of main


void MyTask::stopHook()
{
    canClose(can_port);
} 

void MyTask::startHook()
{
    std::cout << std::endl << "------ startHook ---------" << std::endl;

    // this is rquired to start the PDO mapping
    motor->sendNMT_stateChange(NMT_PRE_OPERATIONAL);


    // we want to receive POSITION_ACTUAL_VALUE and CURRENT_ACTUAL_VALUE packed into PDO1_TX
    // we store these values in a vector-like structure of type PDO_MappingList.
    PDO_MappingList objects_to_map;
    objects_to_map.push_back( POSITION_ACTUAL_VALUE );
    objects_to_map.push_back( CURRENT_ACTUAL_VALUE );

    // map two objects to the PDO1_TX ("TX" means "sent by the slave to the master")
    motor->pdoMapping( PDO1_TX,  objects_to_map);

    // tell the slave to send the PDO1_TX message once for every single synch message
    motor->pdoSetTransmissionType_Synch(PDO1_TX, 1);

    // Synchronous callbacks are not thread safe.
    ev1 = motor->events()->add_subscription(POSITION_ACTUAL_VALUE, CALLBACK_SYNCH, PrintCallback  );

    // Synchronous callbacks are thread safe because are executed inside the thread of the callee.
    ev2 = motor->events()->add_subscription(CURRENT_ACTUAL_VALUE,  CALLBACK_ASYNCH, PrintCallback  );

    motor->sendNMT_stateChange(NMT_OPERATIONAL);
}


void MyTask::updateHook()
{
    std::cout << std::endl << "------ loopHook ---------" << std::endl;
    static int count = 0;

    // note: you should send only 1 sync for each CANPort, not one per motor
    cmi_sendSync();

    // The event associated to POSITION_ACTUAL_VALUE is SYNCHRONOUS, therefore it is called by another thread.

    sleep_for(Milliseconds (5) );

    // Even if we already received CURRENT_ACTUAL_VALUE, the event was configured as ASYNCHRONOUS.
    // For this reason we need to call the method spin() if we want the callback to be executed.

    std::cout << "...and...\n";
    motor->events()->spin();

}



