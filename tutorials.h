#ifndef TUTORIALS_H
#define TUTORIALS_H

#include <string>

#ifdef WIN32
int   DEFAULT_NODE_ID        = 3;
char  CAN_DRIVER[]           = "../lib/driver_peak_win32.dll";
char  BUSNAME[]              = "PCAN_PCIBUS1";
char  OBJ_DICTIONARY_FILE[]  = "../etc/ingenia_venus.eds";
#else
int   DEFAULT_NODE_ID        = 32;
char  CAN_DRIVER[]           = "../lib/libdriver_socket.so";
char  BUSNAME[]              = "can0";
char  OBJ_DICTIONARY_FILE[]  = "../etc/ingenia_venus.eds";
char  NAME_DICTIONARY[]      = "ingenia_venus";
char  MAL_CONFIG_XML[]       = "../etc/PlutoTest-2.xml";

#endif


inline void PrintCallback (ObjectEntry const& entry, ObjectData const& data_obj)
{
  //  std::cout << "-------------------------" << std::endl;
    std::cout << "Your received object with:  " ;
    std::cout << "  INDEX: 0x"    << std::hex  <<   entry.index() ;
    std::cout << "  SUBINDEX: 0x" << std::hex  <<   (int)entry.subindex()   ;
    std::cout << " -> VALUE: 0x" << std::hex <<  data_obj   << std::endl;
  //  std::cout << "------------------------" << std::dec <<  std::endl;
}

#endif // TUTORIALS_H
