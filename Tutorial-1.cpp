/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "cmi/CAN.h"
#include "cmi/CAN_Interface.h"
#include "cmi/CAN_driver.h"
#include "cmi/CMI.h"
#include "cmi/CO301_interface.h"
#include "cmi/CO402_def.h"
#include "tutorials.h"

using namespace CanMoveIt;


int main(int argc, char** argv)
{
    // this is the node that identify the device (slave)
    int DEVICE_NODE_IT = DEFAULT_NODE_ID;

    if (argc == 2)  DEVICE_NODE_IT = atoi(argv[1]);
    if( DEVICE_NODE_IT <1 || DEVICE_NODE_IT>=128)
    {
        printf("I expected a device node between 1 and 127.\n");
        return 1;
    }

    try{
        // load the driver stored in a dynamic library (included with CAN/MoveIt).
        LoadCanDriver( CAN_DRIVER );

        // open the CAN port on you master with the correct name and baudrate
        CANPort *can_port = canOpen( BUSNAME, "1M");

        CreateObjectDictionary( OBJ_DICTIONARY_FILE,   NAME_DICTIONARY);

        CO301_InterfacePtr device = create_CO301_Interface( can_port,
                                                            DEVICE_NODE_IT,
                                                            NAME_DICTIONARY);

        //---------------------------------------------------------------------------

        std::cout << "\n-----------Let's test some write and read -------\n\n";


        // NOTE: on the servo drive side, when you write the object MODE_OPERATION, after _some_ time the
        // object MODE_OPERATION_DISPLAY is updated (remotely).
        // Let's give it a try...
        std::cout << "Send MODE_OPERATION -> " << (int)TORQUE_MODE << std::endl;

        // This method will write the value TORQUE_MODE
        // into the register (object) MODE_OPERATION (see the file CO402_def.h )
        device->sdoWrite( MODE_OPERATION, (Int8)TORQUE_MODE);

        //---------------------------------------------------------------------------

        // In CANopen, when you write the object MODE_OPERATION, shortly the object MODE_OPERATION_DISPLAY
        // is updated. This operation is very fast but requires a certain amount of time that is not negligible.


        // The following command is NOT blocking.
        // This means that we are sending a request from the master to the slave, but we do NOT wait for a reply.
        device->sdoObjectRequest( MODE_OPERATION_DISPLAY );

        // Retrieve the local value of the Object. Note that this value is not necessary the most recent one.
        // May be the
        Int8 current_mode;
        DataStatus ret = device->getLastObjectReceived( MODE_OPERATION_DISPLAY, &current_mode);

        std::cout<< "NEW_DATA = "<<  ret;
        std::cout<< "\t local MODE_OPERATION_DISPLAY has value " << (int)current_mode << std::endl;


        // You will notice that the local value of the MODE_OPERATION_DISPLAY is (probably) different from "4".
        // In fact, we haven't received yet an answer from the servo drive.
        printf("\n sleep some time ...\n\n");

        sleep_for( Milliseconds(10)); // wait 10 milliseconds (it should be more than enough)

        ret = device->getLastObjectReceived(MODE_OPERATION_DISPLAY, &current_mode);

        // now the value is correct, because we received asynchronously the value (probably)
        std::cout<< "NEW_DATA = "<<  ret ;
        std::cout<< "\t local MODE_OPERATION_DISPLAY has value " << (int)current_mode << std::endl;

        //---------------------------------------------------------------------------

        std::cout << "\n-------------- Let's try again... ------------\n\n";

        std::cout << "Send MODE_OPERATION -> " << INTERPOLATED_POSITION_MODE << std::endl;
        device->sdoWrite( MODE_OPERATION , (Int8)INTERPOLATED_POSITION_MODE);

        // this time we use a blocking version, to wait for an answer. Timeout is 100 milliseconds
        ret = device->sdoRequestAndGet( MODE_OPERATION_DISPLAY, &current_mode, Milliseconds(1000) );

        std::cout<< "NEW_DATA = "<<  ret ;
        std::cout<< "\t local MODE_OPERATION_DISPLAY has value " << (int)current_mode << std::endl;

        canClose(can_port);
    }
    catch( Exception& exc)
    {
        std::cout<< exc.displayText() << std::endl;
        return 1;
    }

    return 0;
}
