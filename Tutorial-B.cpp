/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "cmi/CO301_interface.h"
#include "cmi/MAL_CANOpen402.h"
#include "cmi/CMI.h"
#include "OS/PeriodicTask.h"
#include "tutorials.h"

using namespace CanMoveIt;
//---------------------------------------------------
// start defining some constants
//---------------------------------------------------
// lets use a class as container of our periodic controller
class Mirror_PeriodicTask : public PeriodicTask
{
private:
    unsigned cycle;
    MotorControllerPtr motor[2];
    bool ACTIVE;

public:
    void startHook();
    void updateHook();
    void stopHook();
};

float P_param = 50, D_param =100;
bool USE_POSITION = true;

//---------------------------------------------------
void Mirror_PeriodicTask::startHook()
{
    for(int i=0; i<2; i++)
    {
        motor[i] = cmi_getMotorPtr(i);
        // don't forget this
        motor[i]->configureDrive();
    }
    cycle = 0;

    ACTIVE = false;

    motor[0]->setModeOperation( TORQUE_MODE );

    if( USE_POSITION )
    {
        motor[1]->setModeOperation( INTERPOLATED_POSITION_MODE );
        motor[1]->setInterpolatedPositionPeriod( Milliseconds(10) );
    }
    else{  motor[1]->setModeOperation( TORQUE_MODE ); }

    motor[0]->startDrive();
    motor[1]->startDrive();

    // it is best practice to always do this at the end of the startFunc
    cmi_waitMotorsReady();
}

//---------------------------------------------------
void Mirror_PeriodicTask::updateHook()
{
    double actual_pos[2];
    motor[0]->getActualPosition(  &actual_pos[0] );
    motor[1]->getActualPosition(  &actual_pos[1] );

  //  motor[1]->setCurrentTarget( current );

    if( USE_POSITION)
    {
        motor[1]->pushInterpolatedPositionTarget( actual_pos[0] );
            printf("positions: motors %f / %f\n", actual_pos[0], actual_pos[1]);
    }
    else{
        static float prev_error = 0;
        float error   = actual_pos[0] - actual_pos[1];
        float error_d = error - prev_error;
        prev_error = error;
        float current = error*P_param + error_d* D_param;

        float max = 200;
        if( current >  max)  current =  max;
        if( current < -max)  current = -max;

        motor[1]->setCurrentTarget( current );

        if( cycle%10 == 0) printf("positions: motors %f / %f  current %.1f = %.1f + %.1f \n",
                                    actual_pos[0], actual_pos[1],
                                    current, error*P_param, error_d* D_param );
    }

    cycle++;


    cmi_sendSync();
}

void Mirror_PeriodicTask::stopHook()
{
     motor[0]->stopDrive();
     motor[1]->stopDrive();
     printf("stopped\n");
}

//---------------------------------------------------
int main(int argc, char ** argv)
{
    if( argc >= 2) P_param = atof(argv[1]);
    if( argc == 3) D_param = atof(argv[2]);

    CreateObjectDictionary(OBJ_DICTIONARY_FILE, NAME_DICTIONARY);
    // the xml file contain all the setup we need
    cmi_init(CAN_DRIVER, MAL_CONFIG_XML);

    Mirror_PeriodicTask task;
    task.start_execution(Milliseconds(9) );
    task.wait_completion();

    return 0;
}
