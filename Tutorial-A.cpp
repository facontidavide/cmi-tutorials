#include "CO301_interface.h"
#include "MAL_CANOpen402.h"
#include "CMI.h"

using namespace CanMoveIt;
//---------------------------------------------------
// start defining some constants
const TIMEVAL PERIOD_US = 20*1000;//50 Hz period
const double BASE_VELOCITY = 5; //radians/second
const double POSITION_INCREMENT = 20;
//---------------------------------------------------
// lets use a class as container of our periodic controller
class MyPeriodicTask : public PeriodicTask
{
private:

    enum {
        STAND_BY,
        GOING_TO_A,
        GOING_TO_B
    };

    int PHASE;
    unsigned cycle;
    double target_A; // radians
    double target_B; // radians
    double velocity_multiplier; // will go from 1 to 5
    MotorControllerPtr motor;
    void position_reached(EventData);
public:
    void startHook();
    void updateHook();
    void stopHook();
};

//---------------------------------------------------
int main()
{
    CreateObjectDictionary("ingenia_venus.eds", "Ingenia::Venus");
    cmi_init("./libcandriver_socket.so", "VenusTest.xml");

    MyPeriodicTask task;
    task.start_execution(PERIOD_US);
    task.wait_completion();

    return 0;
}
//---------------------------------------------------
void MyPeriodicTask::startHook()
{
    motor = cmi_getMotorPtr(0);
    cycle = 0;
    velocity_multiplier = 1;
    PHASE = STAND_BY;

    // don't forget this
    cmi_configureMotor(motor);

    motor->setModeOperation( PROFILED_POSITION_MODE );

    motor->setProfiledPositionParameter( PP_VELOCITY, BASE_VELOCITY);
    motor->setProfiledPositionParameter( PP_ACCELERATIONS,2000);

    // it is best practice to always do this at the end of the startFunc
    cmi_waitMotorsReady(5000);
}

//---------------------------------------------------
void MyPeriodicTask::updateHook()
{
   // UNS32 digital_input_GP1;
  //  motor->getGeneralPurposeInput(GP1, &digital_input_GP1);
    UNS8 digital_input_GP1 = 1;
    EventData event;

    static CO301_Interface *co301 = cmi_getCO301_Interface(motor);
    co301->sdoRequestRemoteObject( ObjectIndex( 0x2A02, 2 ) );
    UNS16 GPIO;
    co301->getLocalObjectValue<UNS16>(  ObjectIndex( 0x2A02, 2 ), &GPIO);
    digital_input_GP1 = (GPIO >> 4) & 0x1;

    double actual_pos;
    motor->getActualPosition(  &actual_pos );

    // if you were in standby and you detect GP1 is ON, switch to state GOING_TO_B
    if (PHASE == STAND_BY && digital_input_GP1 > 0)
    {
        PHASE = GOING_TO_B;
        target_A = actual_pos;
        target_B = actual_pos + POSITION_INCREMENT;

        // We want the event EVENT_POSITION_REACHED to be pushed info a queue that
        // we will read ourself the FIFO asynchonously.
        motor->events()->configureEvent( EVENT_PROFILED_POSITION_REACHED,
                                   boost::bind( &MyPeriodicTask::position_reached, this, _1),
                                   CALLBACK_ASYNCH);

        printf(">>>> go to %f\n\n",target_B);
        motor->setProfiledPositionTarget(  target_B );
    }
    else if (PHASE != STAND_BY && digital_input_GP1 == 0)
    {
        printf("stop motion\n");
        PHASE = STAND_BY;
        motor->setProfiledPositionTarget(  actual_pos , PP_HALT);
    }
    else // we are either in the states GOING_TO_A or GOING_TO_B
    {
        // pop event until the queue is empty
        motor->events()->spin();
        // if the position reached event is called,we have the phase transition
    }

    cycle++;
    if( cycle%5 == 0) printf("pos: %f\n", actual_pos);
    cmi_sendSync();
}
//---------------------------------------------------
void MyPeriodicTask::position_reached(EventData)
{
    printf("target reached\n");
    if (PHASE == GOING_TO_B)
    {
        printf(">>>> go to %f\n\n",target_A);
        PHASE = GOING_TO_A;
        motor->setProfiledPositionTarget(  target_A );
    }
    else if (PHASE == GOING_TO_A)
    {
        printf(">>>> go to %f\n\n",target_B);
        PHASE = GOING_TO_B;

        if( velocity_multiplier >= 5)   velocity_multiplier  = 1.0;
        else                            velocity_multiplier += 1.0;

        motor->setProfiledPositionParameter( PP_VELOCITY, BASE_VELOCITY*velocity_multiplier);
        motor->setProfiledPositionTarget(  target_B );
    }
}

void MyPeriodicTask::stopHook()
{
    printf("STOP!!!\n");
    motor->setProfiledPositionTarget(  0 , PP_HALT);
    usleep(1000);
}
