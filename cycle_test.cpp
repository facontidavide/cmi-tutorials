/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "OS/Stopwatch.h"
#include "OS/PeriodicTask.h"

using namespace CanMoveIt;

class MyTask: public PeriodicTask
{
    Stopwatch sw;
public:
    MyTask():PeriodicTask()
    {

    }

    void startHook() {  }
    void updateHook()
    {
        static int count =0;

        if (count > 0) sw.stop();
        sw.start();

        if(count++ %100 == 0)
        {
            printf("last: %lld   %lld %lld %lld\n",
                   sw.elapsed().count(),
                   sw.min().count(),
                   sw.average().count(),
                   sw.max().count()) ;
            sw.reset();
        }
    }

    void stopHook() {}
};

int main(int argc, char** argv)
{
    long long int period_usec =  9500;
    if( argc == 2) period_usec = atoi(argv[1]);

    printf("sleep for: %lld\n", period_usec);

    MyTask task;
    task.start_execution(Microseconds(period_usec));
    task.wait_completion();
    /*
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = period_usec*1000;

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    for (int i=0; i<100; i++)
    {
        if(nanosleep(&tim , &tim2) < 0 )
        {
            printf("Nano sleep system call failed \n");
            return -1;
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);

    long long int delay_usec = (end.tv_sec - start.tv_sec)* 1000*1000;
    delay_usec += (end.tv_nsec - start.tv_nsec) / 1000;

    printf("sleept for: %lld\n", delay_usec/100);
*/
    return 0;
}

