
#include "CO301_interface.h"
#include "MAL_CANOpen402.h"
#include "CMI.h"

using namespace CanMoveIt;

//---------------------------------------------------
// start defining some constants
const TIMEVAL PERIOD_US = 10*1000;//100 Hz period
const unsigned MAX_MOTORS = 3;
unsigned NUM_MOTORS = 1;
const int MAX_INDEX = 600;

Mutex mutex;

double ABS( double x)
{
    if (x<0) return -x;
    else return x;
}

//---------------------------------------------------
// lets use a class as container of our periodic controller
class Learner_PeriodicTask : public PeriodicTask
{
private:
    unsigned cycle;
    MotorControllerPtr motor[MAX_MOTORS];

    enum {
        STAND_BY =1,
        LEARNING =2,
        GOING_TO_START =3,
        WAITING_GP2 = 4,
        REPEAT =5
    };

    int PHASE;
    int index;
    int target_reached[MAX_MOTORS];
    double recorded_trajectory[MAX_INDEX][MAX_MOTORS];

    void on_target(EventData e);

public:
    void startHook();
    void updateHook();
    void stopHook()  {}
};

//---------------------------------------------------
int main(int argc, char **argv)
{
    printf("USAGE: Tutorial-D [num_nodes]   (default is 1, maximum is 3\n\n");

    CreateObjectDictionary("ingenia_venus.eds", "Ingenia::Venus");
    cmi_init("./libcandriver_socket.so", "VenusTest.xml");

    if( argc == 2)
    {
        NUM_MOTORS = atoi(argv[1]);
        if (NUM_MOTORS <1)
        {
            NUM_MOTORS = 1;
            printf(" warning: num motors can't be less than 1\n");
        }
        if (NUM_MOTORS >MAX_MOTORS)
        {
            NUM_MOTORS = MAX_MOTORS;
            printf(" warning: num motors can't be more than %d\n",MAX_MOTORS);
        }
    }

    Learner_PeriodicTask task;
    task.start_execution(PERIOD_US);
    task.wait_completion();

    return 0;
}
//---------------------------------------------------
void Learner_PeriodicTask::startHook()
{
    index = 0;
    for(unsigned i=0; i<NUM_MOTORS; i++)
    {
        motor[i] = cmi_getMotorPtr(i);
        motor[i]->configureDrive();
    }
    cycle = 0;

    PHASE = STAND_BY;

    // it is best practice to always do this at the end of the startFunc
    cmi_waitMotorsReady(5000);
}

//---------------------------------------------------
void Learner_PeriodicTask::updateHook()
{

    UNS32 digital_input_GP1 = 1, digital_input_GP2=1;

    static CO301_Interface *co301 = cmi_getCO301_Interface(motor[0]);
    co301->sdoRequestRemoteObject( ObjectIndex( 0x2A02, 2 ) );
    UNS16 GPIO;
    co301->getLocalObjectValue<UNS16>(  ObjectIndex( 0x2A02, 2 ), &GPIO);
    digital_input_GP1 = (GPIO >> 4) & 0x1;
    digital_input_GP2 = (GPIO >> 5) & 0x1;
    //----------------------------------------
    double actual_position[NUM_MOTORS];
    for(unsigned i=0; i<NUM_MOTORS; i++)
    {
        motor[i]->getActualPosition( &actual_position[i]);
    }
    //----------------------------------------
    if(PHASE ==  STAND_BY && digital_input_GP1)
    {
        PHASE = LEARNING;
        printf(">>> STAND_BY switch to phase %d  <<<\n", PHASE);
        for(unsigned i=0; i<NUM_MOTORS; i++)
        {
            motor[i]->setModeOperation( TORQUE_MODE );
            motor[i]->startDrive();
        }
    }
    else if(PHASE !=  STAND_BY && !digital_input_GP1)
    {
        PHASE = STAND_BY;
        for(unsigned i=0; i<NUM_MOTORS; i++)
        {
            motor[i]->stopDrive();
        }
    }

    switch (PHASE)
    {

    case LEARNING:
    {
        for(unsigned i=0; i<NUM_MOTORS; i++)
        {
            motor[i]->setCurrent( 0 );
            recorded_trajectory[index][i] = actual_position[i];
        }

        index++;
        if( index >= MAX_INDEX)
        {
            index=0;
            PHASE = GOING_TO_START;
            printf(">>> LEARNING switch to phase %d  <<<\n", PHASE);
            for(unsigned i=0; i<NUM_MOTORS; i++)
            {
                motor[i]->setModeOperation( PROFILED_POSITION_MODE );
                motor[i]->setProfiledPositionParameter( PP_VELOCITY, 50);
                motor[i]->setProfiledPositionParameter( PP_ACCELERATIONS,20);

                target_reached[i] = false;
                motor[i]->setProfiledPositionTarget( recorded_trajectory[index][i] ); // index==0
                motor[i]->events()->configureEvent( EVENT_PROFILED_POSITION_REACHED,
                                                    boost::bind(&Learner_PeriodicTask::on_target,this, _1),
                                                    CALLBACK_SYNCH);
            }
        }
    }break;

    case GOING_TO_START:
    {
        bool ON_TARGET = true;
        mutex.lock();
        for(unsigned i=0; i<NUM_MOTORS; i++)
        {
            ON_TARGET &= target_reached[i];
        }
        mutex.unlock();

        if( ON_TARGET )
        {
            PHASE = WAITING_GP2;
            printf(">>>GOING_TO_START  switch to phase %d  <<<\n", PHASE);
            for(unsigned i=0; i<NUM_MOTORS; i++)
            {
                motor[i]->setModeOperation( INTERPOLATED_POSITION_MODE );
                motor[i]->setInterpolatedPeriod( this->getPeriod_usec()/1000.0 );
            }
        }
    } break;

    case WAITING_GP2:
    {
        if(digital_input_GP2)
        {
            PHASE = REPEAT;
            printf(">>>WAITING_GP2  switch to phase %d  <<<\n", PHASE);
        }
    }break;

    case REPEAT:
    {
        for(unsigned i=0; i<NUM_MOTORS; i++)
        {
            motor[i]->pushInterpolatedPositionTarget( recorded_trajectory[index][i] );
        }
        index++;
        if( index >= MAX_INDEX)
        {
            this->stop_execution();
        }
    }break;

    }
    //----------------------------------------
    cycle++;
    cmi_sendSync();
    //----------------------------------------
}

void Learner_PeriodicTask::on_target(EventData e)
{
    int i = e.device_id;
    mutex.lock();
    target_reached[i] = true;
    mutex.unlock();
}

