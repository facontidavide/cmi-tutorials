/*CanMoveIt Tutorials 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "cmi/CO301_interface.h"
#include "cmi/MAL_CANOpen402.h"
#include "cmi/CMI.h"
#include "tutorials.h"

using namespace CanMoveIt;
//---------------------------------------------------
// start defining some constants
const double BASE_VELOCITY =  10; //radians/second
const UInt32 CURR_LIMIT    = 300;
double ERROR_THRESHOLD     = 10.0;
bool DONE = false;
int mot = 0;
//---------------------------------------------------

// lets use a class as container of our periodic controller
void positionReachedCallback(EventData)
{
    printf("position reached\n");
    DONE = true;
}


//---------------------------------------------------
int main(int argc, char **argv)
{
    if(argc==2 )
    {
        ERROR_THRESHOLD = atof(argv[1]);
    }
    CreateObjectDictionary(OBJ_DICTIONARY_FILE, NAME_DICTIONARY);
    // the xml file contain all the setup we need
    cmi_init(CAN_DRIVER, MAL_CONFIG_XML);

    MotorControllerPtr motor[2];
    double initial_position[2];
    for (int i=0; i<2; i++)
    {
        motor[i] = cmi_getMotorPtr(i);
        motor[i]->setCurrentLimit( CURR_LIMIT );
        motor[i]->startDrive();

        // remember the initial position.
        initial_position[i] = 0;
        motor[i]->getActualPosition( &initial_position[i] );

        motor[i]->events()->add_subscription(EVENT_PROFILED_POSITION_REACHED,
                                             CALLBACK_ASYNCH,
                                             positionReachedCallback);
    }

    int m = 0;

    while ( 1 )
    {
        printf("start moving motor %d\n", m);
        //start moving
        motor[m]->setModeOperation( PROFILED_VELOCITY_MODE );
        motor[m]->setProfiledVelocityTarget( BASE_VELOCITY );

        bool limit_reached = false;
        DONE = false;

        while(!DONE)
        {
            //cmi_sendSync();

            sleep_for( Milliseconds(100));
            double pos_error = 0;
            motor[m]->requestPositionError();
            motor[m]->getPositionError( &pos_error );
            printf(" %d: pos error %f [%d]\n", m, pos_error, limit_reached);

            // still moving. Check the position error.
            if( !limit_reached )
            {
                if( fabs(pos_error) > ERROR_THRESHOLD )
                {
                    printf("maximum error limit reached\n");
                    limit_reached = true;
                    motor[m]->setModeOperation(  PROFILED_POSITION_MODE );
                    motor[m]->setProfileParameter( PROFILE_VELOCITY, 20);
                    motor[m]->setProfileParameter( PROFILE_ACCELERATION,10);
                    motor[m]->setProfileParameter( PROFILE_DECELERATION,10);
                    motor[m]->setProfiledPositionTarget( initial_position[m] );
                }
            }
            else{
                // execite the event. This might set DONE = true;
                motor[m]->events()->spin();
            }
        } // end while !DONE

        //switch to the other motor
        m = (m+1)%2;

    }// never ending while
    return 0;
}
